#!/usr/bin/env python3
import socket, socketserver, sys, select, ssl
from threading import Thread

PORT=8989
cert="domain.crt"
key="domain.key"

class ThreadSelect(Thread):
    def __init__(self, request, wfile, rfile):
        Thread.__init__(self)
        self.request = request
        self.wfile = wfile
        self.rfile = rfile
        self.inputs = []

    def run(self):
        self.inputs.append(self.rfile)
        while 1:
            r, w, x = select.select(self.inputs, [], [], 1)
            if self.rfile in r:
                data = self.rfile.read1(429496)
                strdata = data.decode()
                if strdata.startswith('CONNECT'):
                    toSend = strdata.split('\r\n', 1)
                    dat = strdata.split(' ')
                    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.sock.connect((dat[1], int(dat[2])))
                    self.inputs.append(self.sock)
                    self.sock.send(bytes(toSend[1], 'UTF-8'))
                else:
                    if len(data) == 0:
                        break
                    self.sock.send(data)
            else:
                data = self.sock.recv(429496)
                if len(data) == 0:
                    break
                self.wfile.write(data)

        self.request.close()
        self.sock.close()

class MainHandler(socketserver.StreamRequestHandler):
    def handle(self):
        a = ThreadSelect(self.request, self.wfile, self.rfile)
        a.start()
        a.join()
        return

class MainServer(socketserver.ThreadingTCPServer):
    allow_reuse_address = True
    def __init__(self, addr=('', PORT)):
        super().__init__(addr, MainHandler)

    def get_request(self):
        (sock, addr) = socketserver.ThreadingTCPServer.get_request(self)
        context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        context.load_cert_chain(certfile=cert, keyfile=key)
        context.check_hostname = False
        kwargs = {"server_side": True}

        sock = context.wrap_socket(sock, **kwargs)
        return (sock, addr)

if __name__ == '__main__':
    server = MainServer()
    server.serve_forever()