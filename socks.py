import socket, sys, ssl

class Socks:
    def __init__(self, addr, user):
        self.addr = addr
        self.user = user

    def create_connection(self, addr):
        context = ssl.create_default_context()
        context.verify_mode = ssl.CERT_REQUIRED
        context.check_hostname = True
        context.load_verify_locations("domain.crt")
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock = context.wrap_socket(sock, server_hostname="%s" % self.addr[0])
        try:
            sock.connect((self.addr[0], int(self.addr[1])))
            sock.send(bytes('CONNECT ' + str(addr[0]) + ' ' + str(addr[1]) + ' ' + self.user + '\r\n', 'utf8'))
        except:
            sock.close()
        return sock

if __name__ == '__main__':
    socks = Socks(('localhost', 8989), 'localhost')
    connection = socks.create_connection(('bot.whatismyipaddress.com', 80))
    connection.send(bytes('GET / HTTP/1.1\r\nHost: bot.whatismyipaddress.com\r\n\r\n', 'utf8'))
    rfile = connection.makefile('rb')
    print(rfile.read().decode())
